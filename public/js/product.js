const METHOD_POST = "POST";
const METHOD_GET = "GET";
const METHOD_DELETE = "DELETE";

$("#btn_create").click(function () {
    $("#form_create").trigger("reset");
    $("#errors li").remove();
});

// Create product ajax
$(document).on("click", '#btn_store', function (e) {
    e.preventDefault();
    $(':reset')
    let formData = document.getElementById('form_create');
    let data = new FormData(formData);
    let url = $(this).data("href");

    ajaxSetup();
    callAjaxForm(url, METHOD_POST, data).done(function (data) {
        if (data['errors']) {
            $("#errors_create li").remove();
            $.each(data['errors'], function (i, item) {
                let li = "<li class='text-danger'>" + item + "</li>";
                $("#errors_create").append(li);
            });
            return;
        } else {
            $('#modal_product_create').modal('hide');
            $('#form_search').submit();
            $('.data').load(' .load_data');
        }
    })
});

// Del product ajax
$(document).on("click", '.btn-delete', function () {
    let id = $(this).data("id");
    let url = $(this).data("href");

    ajaxSetup();
    callAjax(url, METHOD_DELETE, id).done(function (data) {
        $("#tbl_product_result").empty();
        $('#tbl_product_result').append(data);
        // $('.data').load(' .load_data');
    })
});

// Edit product ajax
$(document).on("click", '.btn-edit', function () {
    let id = $(this).data("id");
    let url = $(this).data("href");

    callAjax(url, METHOD_GET, id).done(function (data) {
        $('body').append(data);
        $('#modal_product_update').modal('show');
        //$('body').find("#modal_product_update").remove();
    })
});
$(document).on("click",'#closeUpdate',function (){
    $('#modal_product_update').remove();
})

// Update product ajax
$(document).on("click", '.btn-update', function (e) {
    e.preventDefault();
    let formData = document.getElementById('form_update');
    let data = new FormData(formData);
    let url = $(this).data("href");

    ajaxSetup();
    callAjaxForm(url, METHOD_POST, data).done(function (data) {
        if (data['errors']) {
            $("#errors_update li").remove();
            $.each(data['errors'], function(i, item) {
                var li = "<li class='text-danger'>"+ item + "</li>";
                $("#errors_update").append(li);d
            });
            return;
        } else {
            $("#modal_product_update").modal('hide');
            $('body').find("#modal_product_update").remove();
            $('#form_search').submit();
            $('.data').load(' .load_data');
        }
    })
});

$('#search').keyup(function () {
    let url = $(this).data("href");
    data = {
        category_id: $("#category_id").find(":selected").val(),
        keyword: $(this).val()
    }

    ajaxSetup();
    if (data != '') {
        callAjax(url, METHOD_GET, data).done(function (data) {
            $('table').empty();
            $('table').html(data);
        });
    }

});

