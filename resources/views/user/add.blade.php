@extends('layouts.master')
@section('content')

    <div>
        <form method="POST" action="{{ route('user.store') }}">
            @csrf
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" name="name" class="form-control" >
            </div>

            <div class="form-group">
                <label for="email">Mail</label>
                <input type="email" name="email" class="form-control" >
            </div>

            <div class="form-group">
                <label for="confirm_password">password</label>
                <input type="password" name="password" class="form-control" >
            </div>

            <div class="form-group">
                <label for="confirm_password">re-password</label>
                <input type="password" name="confirm_pass" class="form-control" >
            </div>

            <select class="form-control" multiple="multiple" name="roles[]">
                @foreach($roles as $role)
                    <option value="{{$role->id}}">{{$role->display_name}}</option>
                @endforeach

            </select>

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection
