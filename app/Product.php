<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $fillable = [
        'category_id','name','price','image','description'
    ];

    protected $appends = ['image_link'];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
    public function getProductPaginate()
    {
        return $this->setAppends(['image_link'])->latest()->paginate(5);
    }

    public function syncCategories($categories_id)
    {
        return $this->category()->sync($categories_id);
    }

    public function getImageLinkAttribute() {
        return asset('Admin/images/' . $this->image);
    }


    //search
    public function search($data)
    {
        $name = $data['name'] ?? null;
        $category = $data['category_id'] ?? null;

        return $this->withName($name)->withCategory($category)->latest()->paginate(5);
    }
}
