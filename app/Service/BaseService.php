<?php

namespace App\Service;

class BaseService
{
    public $path = 'Admin/images/';

    public function unlink($name)
    {
        unlink($this->path . $name);
    }
}
