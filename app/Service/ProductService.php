<?php

namespace App\Service;

use App\Product;

class ProductService extends BaseService
{
    public $defaultImageName = 'image.png';

    protected $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function addImage($image)
    {
        $nameImage = $image->getClientOriginalName();
        $image->move($this->path, $nameImage);

        return $nameImage;
    }

    public function compareImage($productImage, $image)
    {
        if ($productImage != $image->getClientOriginalName() && $productImage != $this->defaultImageName)
        {$this->unlink($productImage);
        } else {
            $data['image'] = $this->defaultImageName;
        }
    }

    public function deleteImage($id)
    {
        $product = $this->product->findOrFail($id);
        if ($product->image != $this->defaultImageName) {
            $this->unlink($product->image);
        }
    }

    public function search($category_id, $keyword)
    {
        return Product::Where('name', 'LIKE', "%$keyword%")->where('category_id', $category_id)->get();
    }
}
