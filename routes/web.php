<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('home', 'HomeController@index')->name('home');
Route::middleware(['auth'])->group(function () {
    Route::prefix('category')->middleware('checklogin::class')->group(function () {
        Route::get('index', 'CategoryController@index')->name('category.index')
            ->middleware('checkper:category-list');
        Route::get('create', 'CategoryController@create')->name('category.create')
            ->middleware('checkper:category-add');
        Route::post('store', 'CategoryController@store')->name('category.store');
        Route::get('edit/{id}', 'CategoryController@edit')->name('category.edit')
            ->middleware('checkper:category-edit');
        Route::post('update/{id}', 'CategoryController@update')->name('category.update');
        Route::get('destroy/{id}', 'CategoryController@destroy')->name('category.destroy')
            ->middleware('checkper:category-delete');
    });

    Route::prefix('product')->middleware('checklogin::class')->group(function () {
        Route::get('index', 'ProductController@index')->name('product.index')
            ->middleware('checkper:product-list');
        Route::post('store', 'ProductController@store')->name('product.store')
            ->middleware('checkper:product-add');
        Route::get('edit/{id}', 'ProductController@edit')->name('product.edit')
            ->middleware('checkper:product-edit');
        Route::put('update/{id}', 'ProductController@update')->name('product.update');
        Route::delete('destroy/{id}', 'ProductController@destroy')->name('product.destroy')
            ->middleware('checkper:product-delete');
        Route::get('search', 'ProductController@search')->name('product.search')
            ->middleware('checkper:product-search');
    });

    Route::prefix('users')->group(function () {
        Route::get('/','UserController@index')->name('user.index')
            ->middleware('checkper:user-list');
        Route::get('/create','UserController@create')->name('user.create')
            ->middleware('checkper:user-add');
        Route::post('/create','UserController@store')->name('user.store');
        Route::get('/edit/{id}','UserController@edit')->name('user.edit')
            ->middleware('checkper:user-edit');
        Route::post('/edit/{id}','UserController@update')->name('user.update');
        Route::get('/delete/{id}','UserController@destroy')->name('user.destroy')
            ->middleware('checkper:user-delete');
    });

    Route::prefix('permission')->group(function () {
        Route::get('/', 'PermissionController@index')->name('permission.index');
        Route::get('create', 'PermissionController@create')->name('permission.create');
        Route::post('store', 'PermissionController@store')->name('permission.store');
        Route::get('edit/{id}', 'PermissionController@edit')->name('permission.edit');
        Route::post('update/{id}', 'PermissionController@update')->name('permission.update');
        Route::get('destroy/{id}', 'PermissionController@destroy')->name('permission.destroy');
    });

    Route::prefix('roles')->group(function () {
        Route::get('/', 'RolesController@index')->name('role.index')
            ->middleware('checkper:role-list');
        Route::get('create', 'RolesController@create')->name('role.create')
            ->middleware('checkper:role-add');
        Route::post('store', 'RolesController@store')->name('role.store');
        Route::get('edit/{id}', 'RolesController@edit')->name('role.edit')
            ->middleware('checkper:role-edit');
        Route::post('update/{id}', 'RolesController@update')->name('role.update');
        Route::get('destroy/{id}', 'RolesController@destroy')->name('role.destroy')
            ->middleware('checkper:role-delete');
    });

});
