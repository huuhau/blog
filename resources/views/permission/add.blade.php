@extends('layouts.master')
@section('content')
    <div>
        <form method="POST" action="{{ route('permission.store') }}">
            @csrf
            <div class="form-group">
                <label>Permission Name</label>
                <input type="text" name="name" class="form-control" >
            </div>
            <div class="form-group">
                <label>Display Name</label>
                <input type="text" name="display_name" class="form-control" >
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection
