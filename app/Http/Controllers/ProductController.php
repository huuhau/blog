<?php

namespace App\Http\Controllers;

use App\Category;
//use App\Http\Requests\ProductRequest;
use App\Product;
use App\Service\ProductService;
use App\Http\Requests\ProductRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    protected $product;

    protected $productService;

    public function __construct(Category $category,Product $product, ProductService $productService)
    {
        $this->category = $category;
        $this->product = $product;
        $this->productService = $productService;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = $this->category->all();
        $products = Product::all();

        return view('product.index', compact('products', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $data = $request->all();
        $data['image'] = $request->hasFile('image') ? $this->productService->addImage($request->image) :
            $this->productService->defaultImageName;

        $this->product->create($data);

        return response()->json(["status" => 200]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = $this->product->findOrFail($id);
        $categories = $this->product->category();
        return view('product.update', [
            'product' => $product,
            'categories'=>$categories,
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, $id)
    {
        $data = $request->all();
        $product = $this->product->findOrFail($id);
        if ($request->hasFile('image')) {
            $this->productService->compareImage($product->image, $request->image);
            $data['image'] = $this->productService->addImage($request->image);
        }
        $product->update($data);

        return response()->json(["status" => 200]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->productService->deleteImage($id);
        $this->product->destroy($id);
        $products = $this->product->all();

        return view('product.list', [
            'products' => $products,
        ]);
    }

    public function search(Request $request)
    {
        $categories = $this->category->where('name');
        $category_id = $request->get('category_id');
        $keyword = $request->get('keyword');
        $products = $this->productService->search($category_id, $keyword);
        return view('product.list', [
            'products' => $products,
            'categories'=>$categories,
        ]);

    }
}
