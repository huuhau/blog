@extends('layouts.master')
@section('content')

    <div>
        <form method="post" action="{{ route('category.update',$category->id) }}">
            @csrf
            <div class="form-group">
                <label>Category Name</label>
                <input type="text" name="name" class="form-control" value="{{ $category->name }}">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>

    </div>
@endsection
