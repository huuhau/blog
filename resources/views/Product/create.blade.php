<div class="modal" id="modal_product_create" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="form_create" enctype="multipart/form-data">
                <div class="modal-header">
                    <h5 class="modal-title">Create Product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <ul id="errors_create"></ul>
                    <div class="row">
                        <div class="col-8 form-group">
                            <div>
                                <label>Name <sup class="text-danger">(*)</sup></label>
                                <input type="text" name="name" class="form-control">
                            </div>
                            <div>
                                <label>Price <sup class="text-danger">(*)</sup></label>
                                <input type="text" id="price" name="price" class="form-control">
                            </div>
                            <div>
                                <label>Category <sup class="text-danger">(*)</sup></label>
                                <select name="category_id" class="custom-select">
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Description<sup class="text-danger">(*)</sup></label>
                                <textarea name="description" class="form-control"
                                          rows="3"></textarea>
                            </div>
                        </div>
                        <div class="col-4 form-group">
                            <div>
                                <label class="col-form-label">Image</label>
                                <div>
                                    <input type="file" accept="image/*" name="image" id="image"
                                           onchange="showImage(event)" style="display: none;">
                                    <img height="120" width="120" id="output">
                                    <label class="form-label" for="image" style="cursor: pointer;">
                                        <p class="btn btn-outline-success">
                                            <i class="fa fa-upload"></i>
                                        </p>
                                    </label>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btn_store"  data-href="{{ route('product.store') }}"
                            class="btn btn-primary">
                        Submit
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
